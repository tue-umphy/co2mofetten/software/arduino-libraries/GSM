#pragma once

#include <Arduino.h>

/*

  Debug macros

*/
#define GSMDF(s)                                                              \
  {                                                                           \
    Serial.print(F(s));                                                       \
  };
#define GSMDP(s)                                                              \
  {                                                                           \
    Serial.print(s);                                                          \
  };
#define GSMDH(s)                                                              \
  {                                                                           \
    GSMDF("0x");                                                              \
    Serial.print(s, HEX);                                                     \
  };
#define GSMDh(s)                                                              \
  {                                                                           \
    Serial.print(s, HEX);                                                     \
  };
#define GSMDL()                                                               \
  {                                                                           \
    Serial.println();                                                         \
  };

#if GSM_DEBUG
#define GSMDBG(cmd)                                                           \
  {                                                                           \
    GSMDF("GSM: ");                                                           \
    cmd;                                                                      \
    GSMDL();                                                                  \
    Serial.flush();                                                           \
  };
#else // #if GSM_DEBUG
#define GSMDBG(cmd)
#endif // #if GSM_DEBUG

#define GSMDS(s) GSMDBG(GSMDF(s))
#define GSMDSN(s, n) GSMDBG(GSMDF(s); GSMDP(n))
#define GSMDSNS(s1, n, s2) GSMDBG(GSMDF(s1); GSMDP(n); GSMDF(s2))
#define GSMDSNSN(s1, n1, s2, n2)                                              \
  GSMDBG(GSMDF(s1); GSMDP(n1); GSMDF(s2); GSMDP(n2))
#define GSMDSNSNS(s1, n1, s2, n2, s3)                                         \
  GSMDBG(GSMDF(s1); GSMDP(n1); GSMDF(s2); GSMDP(n2); GSMDF(s3))
#define GSMDSNSNSH(s1, n1, s2, n2, s3, n3)                                    \
  GSMDBG(GSMDF(s1); GSMDP(n1); GSMDF(s2); GSMDP(n2); GSMDF(s3); GSMDH(n3))
#define GSMDSH(s, n) GSMDBG(GSMDF(s); GSMDH(n))
#define GSMDSHS(s1, n, s2) GSMDBG(GSMDF(s1); GSMDH(n); GSMDF(s2))
#define GSMDSHSH(s1, n1, s2, n2)                                              \
  GSMDBG(GSMDF(s1); GSMDH(n1); GSMDF(s2); GSMDH(n2))
#define GSMDSHSN(s1, n1, s2, n2)                                              \
  GSMDBG(GSMDF(s1); GSMDH(n1); GSMDF(s2); GSMDP(n2))
#define GSMDSNSH(s1, n1, s2, n2)                                              \
  GSMDBG(GSMDF(s1); GSMDP(n1); GSMDF(s2); GSMDH(n2))
#define GSMDSHSHS(s1, n1, s2, n2, s3)                                         \
  GSMDBG(GSMDF(s1); GSMDH(n1); GSMDF(s2); GSMDH(n2); GSMDF(s3))
#define GSMDSHSNS(s1, n1, s2, n2, s3)                                         \
  GSMDBG(GSMDF(s1); GSMDH(n1); GSMDF(s2); GSMDP(n2); GSMDF(s3))
#define GSMDSNSHS(s1, n1, s2, n2, s3)                                         \
  GSMDBG(GSMDF(s1); GSMDP(n1); GSMDF(s2); GSMDH(n2); GSMDF(s3))
#define GSMDSNSNSN(s1, n1, s2, n2, s3, n3)                                    \
  GSMDBG(GSMDF(s1); GSMDP(n1); GSMDF(s2); GSMDP(n2); GSMDF(s3); GSMDP(n3))
#define GSMDSHSNSN(s1, n1, s2, n2, s3, n3)                                    \
  GSMDBG(GSMDF(s1); GSMDH(n1); GSMDF(s2); GSMDP(n2); GSMDF(s3); GSMDP(n3))
#define GSMDSHSHSNS(s1, n1, s2, n2, s3, n3, s4)                               \
  GSMDBG(GSMDF(s1); GSMDH(n1); GSMDF(s2); GSMDH(n2); GSMDF(s3); GSMDP(n3);    \
         GSMDF(s4))
